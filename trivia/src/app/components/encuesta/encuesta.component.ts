import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Preguntas } from 'src/app/model/preguntas';
import { Resultado } from 'src/app/model/Resultado';
import { DatosService } from 'src/app/service/datos.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {
  nombre = "";
  preguntas:Preguntas;
  resultados: Resultado[]; 

  profileForm = new FormGroup({
  });

  constructor(
    private servicio :DatosService
  ) { 
  }

  ngOnInit(): void {
    console.log("encuesta")
    this.servicio.getData().subscribe(data =>{
      console.log(data)
      this.preguntas = data;
    })
  }
  respuestas(valor:boolean){
    if( this.respuestas === undefined ){
      
    }
    console.log(this.preguntas)
  }

}
