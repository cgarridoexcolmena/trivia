import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Preguntas } from '../model/preguntas';

@Injectable({
  providedIn: 'root'
})
export class DatosService {
  private url = "https://5f7cd2bc834b5c0016b059ee.mockapi.io/workshop/v1/questions";

  constructor(private http: HttpClient) { }

  getData(){
    return this.http.get<Preguntas>(this.url);
  }
}
