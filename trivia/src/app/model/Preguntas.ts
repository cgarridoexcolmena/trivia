import { Respuestas } from './Respuestas';

export class Preguntas{
    id: string;
    question: string;
    answers:Respuestas[];
}