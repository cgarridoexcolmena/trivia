import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EncuestaComponent } from './components/encuesta/encuesta.component';
import { ReporteComponent } from './components/reporte/reporte.component';

const routes: Routes = [
  {
  path: "",
  component: EncuestaComponent
  },
  {
    path:"reporte",
    component:ReporteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
